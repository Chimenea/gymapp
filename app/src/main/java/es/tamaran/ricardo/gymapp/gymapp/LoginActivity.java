package es.tamaran.ricardo.gymapp.gymapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button signInButton = findViewById(R.id.buttonLoginSignIn);
        sp = getSharedPreferences("user",MODE_PRIVATE);

        if(sp.getBoolean("logged",false)){
            goToMain();
        }

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginAttempt(v);
            }
        });
    }

    private void loginAttempt(View v) {
        EditText loginUser = findViewById(R.id.editTextLoginUser);
        EditText loginPass = findViewById(R.id.editTextLoginPassword);
        boolean validUser = isValidUser(loginUser);
        boolean validPass = isValidPass(loginPass);
        if(validUser && validPass){
            sp.edit().putBoolean("logged", true).apply();
            goToMain();
        }
    }

    private void goToMain(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    private boolean isValidUser(EditText loginUser){
        boolean empty = TextUtils.isEmpty(loginUser.getText().toString().trim());
        if(empty){
            loginUser.setError(getText(R.string.error_field_required));
        }
        return !empty;
    }

    private boolean isValidPass(EditText loginPass){
        boolean empty = TextUtils.isEmpty(loginPass.getText().toString().trim());
        boolean tooShort = loginPass.getText().toString().length() < 5;
        if(empty){
            loginPass.setError(getText(R.string.error_field_required));
        }else{
            if(tooShort){
                loginPass.setError(getText(R.string.error_invalid_password));
            }
        }
        return !empty && !tooShort;
    }
}
